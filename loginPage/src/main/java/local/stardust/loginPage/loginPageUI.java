package local.stardust.loginPage;

import javax.servlet.annotation.WebServlet;


import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class loginPageUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();
        initBackground();
        layout.setHeight("100%");
        /*final TextField name = new TextField();
        name.setCaption("Type your name here:");

        Button button = new Button("Click Me");
        button.addClickListener(e -> {
            layout.addComponent(new Label("Thanks " + name.getValue() 
                    + ", it works!"));
        });
        
        layout.addComponents(name, button);*/
        final Panel panel = new Panel("Welcome to Stardust!");
        layout.addComponent(panel);
    	FormLayout form = new FormLayout();
		TextField username = new TextField("Username");
		form.addComponent(username);
		PasswordField password = new PasswordField("Password");
		form.addComponent(password);
		Button send = new Button("Entra");
		send.addClickListener(new ClickListener() {
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				if(true)
				{
					Notification.show("Eccolo!", Notification.Type.HUMANIZED_MESSAGE);
				}
				else
				{
					Notification.show("Credenziali non valide", Notification.Type.ERROR_MESSAGE);
				}
			}
		
			
		});
		panel.setSizeUndefined();
		form.setMargin(true);
		panel.setContent(form);
		form.addComponent(send);
        setContent(layout);
        layout.setComponentAlignment(panel, Alignment.MIDDLE_CENTER);
        
    }

    private void initBackground() {
        Page.getCurrent().getStyles().add(
                ".v-ui { background: url(background.jpg) no-repeat center center fixed;" +
                        "-webkit-background-size: cover;" +
                        "-moz-background-size: cover;" +
                        "-o-background-size: cover;" +
                        "background-size: cover; }"
        );
    }
    
    @WebServlet(urlPatterns = "/*", name = "loginPageUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = loginPageUI.class, productionMode = false)
    public static class loginPageUIServlet extends VaadinServlet {
    }
}
