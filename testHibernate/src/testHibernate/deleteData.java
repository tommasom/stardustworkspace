package testHibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class deleteData {
	 public static void main(String args[]) {
	        Configuration cfg = new Configuration();
	        cfg.configure("hibernate.cfg.xml");// populates the data of the
	                                            // configuration file

	        // creating session factory object
	        SessionFactory factory = cfg.buildSessionFactory();

	        // creating session object
	        Session session = factory.openSession();

	        // creating transaction object
	        Transaction t = session.beginTransaction();

		 
		 
		 Employee e = new Employee();
	 e.setId(116);	session.delete(e);
	 t.commit();
	 session.close();
	 }
}
