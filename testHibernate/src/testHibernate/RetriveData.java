package testHibernate;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.mapping.List;

public class RetriveData {
    public static void main(String args[]) {
        Configuration cfg = new Configuration();
        cfg.configure("hibernate.cfg.xml");// populates the data of the
                                            // configuration file

        // creating session factory object
        SessionFactory factory = cfg.buildSessionFactory();

        // creating session object
        Session session = factory.openSession();

        // creating transaction object
        Transaction t = session.beginTransaction();

        java.util.List<Employee> list = session.createQuery( "from Employee where firstname= :fn").setString("fn","pippo").list();
        
        
      //  Query<?> query = session.createQuery("from Employee");
      //  java.util.List list = query.list();
        for (int i = 0; i < list.size(); i++) {
        	   Employee user = list.get(i);
        	   System.out.println(user.getFirstName());
        	  }
        t.commit();
        session.close();
    }
}
